package com.senkou.heroquestmanager

import android.app.Application
import com.senkou.heroquestmanager.data.local.RoomRepository
import com.senkou.heroquestmanager.data.room.databases.CampaniasDB
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class MyApplication : Application() {

    private val applicationScope = CoroutineScope(SupervisorJob())
    val roomProvider by lazy { RoomRepository(CampaniasDB.getDatabase(this, applicationScope)) }
}