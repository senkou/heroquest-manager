package com.senkou.heroquestmanager.domain.model

import com.senkou.heroquestmanager.data.model.Clases
import com.senkou.heroquestmanager.data.model.Sexo

data class PersonajeCreado(val nombre: String, val clase: Clases, val sexo: Sexo)