package com.senkou.heroquestmanager.domain.model

import android.graphics.drawable.Drawable

data class TiradaDado(var valor: Int, var imagen: Drawable)
