package com.senkou.heroquestmanager.domain.model

data class PersonajeUI(
    val id: Int = 0,
    val nombreJugador: String,
    val idCampania: Int = 0,
    val idClase: Int,
    val sexo: String,
    var vida: Int,
    var mente: Int,
    var ataque: Int,
    var defensa: Int,
    var oro: Int = 0,
    val equipo: List<ObjetoEquipoUI> = emptyList()
)
