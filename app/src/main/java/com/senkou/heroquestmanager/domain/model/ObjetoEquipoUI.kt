package com.senkou.heroquestmanager.domain.model

import com.senkou.heroquestmanager.data.room.entity.Equipo

data class ObjetoEquipoUI(
    val idObjeto: Int,
    val idPersonaje: Int,
    val recursoNombre: Int = 0,
    val recursoDrawable: Int = 0,
) {
    fun toEquipo() = Equipo(idPersonaje = idPersonaje, idObjeto = idObjeto)
}
