package com.senkou.heroquestmanager.domain.funcionalidad

import com.senkou.heroquestmanager.data.room.entity.Campania
import com.senkou.heroquestmanager.data.room.entity.Equipo
import com.senkou.heroquestmanager.data.room.entity.Objeto
import com.senkou.heroquestmanager.data.room.entity.Personaje
import com.senkou.heroquestmanager.domain.model.CampaniaUI
import com.senkou.heroquestmanager.domain.model.ObjetoEquipoUI
import com.senkou.heroquestmanager.domain.model.PersonajeUI
import com.senkou.heroquestmanager.ui.soundId
import com.senkou.heroquestmanager.ui.sp

fun playSonidoBoton() {
    soundId?.let { sp.play(it, 1.0F, 1.0F, 1, 0, 1.0F) }
}

fun Boolean?.orTrue(): Boolean {
    return this ?: true
}

//region Campania
fun CampaniaUI.toCampania() = Campania(id, nombre, barbaro, enano, elfo, mago)

fun Campania.toCampaniaUI() = CampaniaUI(
    id = id,
    nombre = nombre,
    barbaro = barbaro,
    enano = enano,
    elfo = elfo,
    mago = mago,
)

fun Map<Campania, Map<Personaje, List<Equipo>>>.toCampaniaUI(): CampaniaUI {
    val campania = keys.first()

    val personajes: MutableList<PersonajeUI> = mutableListOf()

    this[campania]?.forEach { map ->
        val personaje = map.key
        val equipo = map.value.map { ObjetoEquipoUI(it.idObjeto, it.idPersonaje) }

        personajes.add(
            PersonajeUI(
                id = personaje.id,
                nombreJugador = personaje.nombreJugador,
                idCampania = campania.id,
                idClase = personaje.idClase,
                sexo = personaje.sexo,
                vida = personaje.vida,
                mente = personaje.mente,
                ataque = personaje.ataque,
                defensa = personaje.defensa,
                oro = personaje.oro,
                equipo = equipo
            )
        )
    }

    return CampaniaUI(
        id = campania.id,
        nombre = campania.nombre,
        barbaro = campania.barbaro,
        enano = campania.enano,
        elfo = campania.elfo,
        mago = campania.mago,
        personajes = personajes
    )
}
//endregion

//region Personaje
fun Personaje.toPersonajeUI(
    equipo: List<Objeto> = emptyList()
): PersonajeUI {
    return PersonajeUI(
        id = id,
        nombreJugador = nombreJugador,
        idCampania = idCampania,
        idClase = idClase,
        sexo = sexo,
        vida = vida,
        mente = mente,
        ataque = ataque,
        defensa = defensa,
        oro = oro,
        equipo = equipo.map {
            ObjetoEquipoUI(
                it.id,
                id,
                it.recursoNombre,
                it.recursoDrawable
            )
        }
    )
}

fun List<PersonajeUI>.toPersonajes() = map { personaje ->
    Personaje(
        nombreJugador = personaje.nombreJugador,
        idCampania = personaje.idCampania,
        idClase = personaje.idClase,
        sexo = personaje.sexo,
        vida = personaje.vida,
        mente = personaje.mente,
        ataque = personaje.ataque,
        defensa = personaje.defensa,
        oro = personaje.oro
    )
}

fun PersonajeUI.toPersonaje() = Personaje(
    nombreJugador = nombreJugador,
    idCampania = idCampania,
    idClase = idClase,
    sexo = sexo,
    vida = vida,
    mente = mente,
    ataque = ataque,
    defensa = defensa,
    oro = oro
)

fun PersonajeUI.toEquipo() = equipo.map { it.toEquipo().copy(idPersonaje = id) }
//endregion


