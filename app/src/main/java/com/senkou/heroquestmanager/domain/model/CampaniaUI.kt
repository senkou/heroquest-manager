package com.senkou.heroquestmanager.domain.model

data class CampaniaUI(
    val id: Int = 0,
    val nombre: String,
    val barbaro: Boolean,
    val enano: Boolean,
    val elfo: Boolean,
    val mago: Boolean,
    val personajes: List<PersonajeUI> = mutableListOf(),
)

