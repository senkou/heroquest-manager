package com.senkou.heroquestmanager.data.room.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.senkou.heroquestmanager.data.room.entity.Objeto
import com.senkou.heroquestmanager.data.room.entity.Personaje
import kotlinx.coroutines.flow.Flow

@Dao
interface PersonajeDAO {

    @Query("SELECT * FROM personaje")
    suspend fun getAll(): List<Personaje>

    @Query("SELECT * FROM personaje where id = (:id)")
    suspend fun getById(id: Int): Personaje

    @Query(
        "SELECT * FROM personaje p " +
                " left join equipo e on e.IdPersonaje = p.Id " +
                " left join objeto o on o.Id = e.IdObjeto " +
                " where p.IdCampania = (:id)"
    )
    fun getAllByCampaniaId(id: Int): Flow<Map<Personaje, List<Objeto>>>

    @Query("SELECT * FROM personaje where IdCampania = (:idCampania)")
    fun getByCampaniaIdFlow(idCampania: Int): Flow<List<Personaje>>

    @Query("SELECT * FROM personaje where IdCampania = (:idCampania)")
    suspend fun getByCampaniaId(idCampania: Int): List<Personaje>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(personaje: Personaje): Long

    @Update
    suspend fun update(personaje: Personaje)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(personajes: List<Personaje>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(vararg personaje: Personaje)

    @Delete
    suspend fun delete(personaje: Personaje)

    @Query("delete FROM personaje where IdCampania = (:idCampania)")
    suspend fun deleteAllByCampania(idCampania: Int)
}
