package com.senkou.heroquestmanager.data.room.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.senkou.heroquestmanager.data.room.entity.Campania
import com.senkou.heroquestmanager.data.room.entity.Equipo
import com.senkou.heroquestmanager.data.room.entity.Personaje
import kotlinx.coroutines.flow.Flow

@Dao
interface CampaniaDAO {
    @Query("SELECT * FROM campania")
    fun getAll(): Flow<List<Campania>>

    @Query("SELECT * FROM campania where id = (:id)")
    suspend fun getById(id: Int): Campania?

    @Query(
        "SELECT * FROM campania c " +
                " join personaje p on p.IdCampania = c.id " +
                " left join equipo e on e.idPersonaje = p.id " +
                " where c.id = (:id)"
    )
    suspend fun getCampaniaToExport(id: Int): Map<Campania, Map<Personaje, List<Equipo>>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(campania: Campania)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(campanias: List<Campania>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(vararg campania: Campania)

    @Delete
    suspend fun delete(campania: Campania)
}