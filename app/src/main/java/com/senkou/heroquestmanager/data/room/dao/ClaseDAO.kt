package com.senkou.heroquestmanager.data.room.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.senkou.heroquestmanager.data.room.entity.Clase
import kotlinx.coroutines.flow.Flow

@Dao
interface ClaseDAO {
    @Query("SELECT * FROM clase")
    fun getAll(): Flow<List<Clase>>

    @Query("SELECT * FROM clase where id = (:id)")
    suspend fun getById(id: Int): Clase

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(clase: Clase)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(clases: List<Clase>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(vararg clase: Clase)

    @Delete
    suspend fun delete(clase: Clase)
}