package com.senkou.heroquestmanager.data.model

enum class Sexo {
    HOMBRE,
    MUJER,
}