package com.senkou.heroquestmanager.data.local

import com.senkou.heroquestmanager.data.room.entity.Campania
import com.senkou.heroquestmanager.data.room.entity.Clase
import com.senkou.heroquestmanager.domain.model.CampaniaUI
import kotlinx.coroutines.flow.Flow

interface CampaniasService {
    val campanias: Flow<List<Campania>>
    suspend fun getClaseById(idClase: Int): Clase
    suspend fun existsCampania(id: Int): Boolean
    suspend fun saveCampania(campania: CampaniaUI)
    suspend fun importCampania(campania: CampaniaUI, callback: () -> Unit)
    suspend fun exportCampania(idCampania: Int): CampaniaUI
    suspend fun deleteCampania(id: Int)
}