package com.senkou.heroquestmanager.data.room.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.senkou.heroquestmanager.data.room.entity.Equipo
import kotlinx.coroutines.flow.Flow

@Dao
interface EquipoDAO {
    @Query("SELECT * FROM equipo")
    suspend fun getAll(): List<Equipo>

    @Query("SELECT * FROM equipo where IdPersonaje = (:idPersonaje)")
    fun getByPersonajeId(idPersonaje: Int): Flow<List<Equipo>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(equipo: Equipo)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(equipos: List<Equipo>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(vararg equipo: Equipo)

    @Delete
    suspend fun delete(equipo: Equipo)

    @Query("delete FROM equipo where IdPersonaje = (:idPersonaje)")
    suspend fun deleteAll(idPersonaje: Int)
}