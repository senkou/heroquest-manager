package com.senkou.heroquestmanager.data.model

enum class Clases(val id: Int) {
    BARBARO(1),
    ENANO(2),
    ELFO(3),
    MAGO(4),

}