package com.senkou.heroquestmanager.data.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Campania(
    @PrimaryKey
    @ColumnInfo(name = "Id")
    var id: Int,

    @ColumnInfo(name = "Nombre")
    val nombre: String,

    @ColumnInfo(name = "HayBarbaro")
    val barbaro: Boolean,

    @ColumnInfo(name = "HayEnano")
    val enano: Boolean,

    @ColumnInfo(name = "HayElfo")
    val elfo: Boolean,

    @ColumnInfo(name = "HayMago")
    val mago: Boolean,
)