package com.senkou.heroquestmanager.data.room.databases

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.senkou.heroquestmanager.data.model.CargadorInicial
import com.senkou.heroquestmanager.data.room.dao.CampaniaDAO
import com.senkou.heroquestmanager.data.room.dao.ClaseDAO
import com.senkou.heroquestmanager.data.room.dao.EquipoDAO
import com.senkou.heroquestmanager.data.room.dao.ObjetoDAO
import com.senkou.heroquestmanager.data.room.dao.PersonajeDAO
import com.senkou.heroquestmanager.data.room.entity.Campania
import com.senkou.heroquestmanager.data.room.entity.Clase
import com.senkou.heroquestmanager.data.room.entity.Equipo
import com.senkou.heroquestmanager.data.room.entity.Objeto
import com.senkou.heroquestmanager.data.room.entity.Personaje
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Database(
    entities = [
        Campania::class,
        Personaje::class,
        Equipo::class,
        Objeto::class,
        Clase::class,
    ],
    version = 1,
    exportSchema = true,
)
abstract class CampaniasDB : RoomDatabase() {
    abstract fun campaniaDAO(): CampaniaDAO
    abstract fun personajeDAO(): PersonajeDAO
    abstract fun equipoDAO(): EquipoDAO
    abstract fun objetoDAO(): ObjetoDAO
    abstract fun claseDAO(): ClaseDAO

    private class CampaniasDBCallback(
        private val scope: CoroutineScope
    ) : Callback() {

        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            INSTANCE?.let { database ->
                scope.launch {
                    database.claseDAO().insertAll(CargadorInicial.clasesIniciales)
                    database.objetoDAO().insertAll(CargadorInicial.objetosIniciales)
                }
            }
        }
    }

    companion object {
        @Volatile
        private var INSTANCE: CampaniasDB? = null

        fun getDatabase(
            context: Context,
            scope: CoroutineScope
        ): CampaniasDB {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    CampaniasDB::class.java,
                    "word_database"
                )
                    .addCallback(CampaniasDBCallback(scope))
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}