package com.senkou.heroquestmanager.data.local

import com.senkou.heroquestmanager.data.room.entity.Clase
import com.senkou.heroquestmanager.data.room.entity.Objeto
import com.senkou.heroquestmanager.data.room.entity.Personaje
import kotlinx.coroutines.flow.Flow

interface PersonajesService {

    val objetos: Flow<List<Objeto>>
    suspend fun getClaseById(idClase: Int): Clase
    suspend fun getPersonajeDb(idPersonaje: Int): Personaje
    fun getPersonajes(idCampania: Int): Flow<Map<Personaje, List<Objeto>>>
    suspend fun updatePersonaje(personaje: Personaje)
    suspend fun anadirEquipo(idPersonaje: Int, idObjeto: Int)
    suspend fun borrarEquipo(idPersonaje: Int, idObjeto: Int)

}