package com.senkou.heroquestmanager.data.room.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.senkou.heroquestmanager.data.room.entity.Objeto
import kotlinx.coroutines.flow.Flow

@Dao
interface ObjetoDAO {
    @Query("SELECT * FROM objeto")
    fun getAll(): Flow<List<Objeto>>

    @Query("SELECT * FROM objeto where id = (:id)")
    suspend fun getById(id: Int): Objeto

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert( objeto: Objeto)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(objetos: List<Objeto>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(vararg objeto: Objeto)

    @Delete
    suspend fun delete(objeto: Objeto)
}
