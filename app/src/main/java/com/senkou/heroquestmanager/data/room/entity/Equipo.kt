package com.senkou.heroquestmanager.data.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index

@Entity(
    indices = [
        Index(
            value = ["IdPersonaje", "IdObjeto"],
            unique = true
        ),
        Index(value = ["IdPersonaje"]),
        Index(value = ["IdObjeto"])
    ],
    primaryKeys = ["IdPersonaje", "IdObjeto"],
    foreignKeys = [
        ForeignKey(
            entity = Personaje::class,
            parentColumns = arrayOf("Id"),
            childColumns = arrayOf("IdPersonaje"),
            onDelete = ForeignKey.SET_NULL
        ),
        ForeignKey(
            entity = Objeto::class,
            parentColumns = arrayOf("Id"),
            childColumns = arrayOf("IdObjeto"),
            onDelete = ForeignKey.SET_NULL
        )
    ],
)
data class Equipo(
    @ColumnInfo(name = "IdPersonaje") //FK
    var idPersonaje: Int,

    @ColumnInfo(name = "IdObjeto") //FK
    var idObjeto: Int,
)
