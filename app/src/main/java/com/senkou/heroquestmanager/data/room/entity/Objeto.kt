package com.senkou.heroquestmanager.data.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Objeto(
    @PrimaryKey
    @ColumnInfo(name = "Id")
    val id: Int,

    @ColumnInfo(name = "RecursoNombre")
    val recursoNombre: Int,

    @ColumnInfo(name = "RecursoDrawable")
    val recursoDrawable: Int,
)
