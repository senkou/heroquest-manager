package com.senkou.heroquestmanager.data.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Clase(
    @PrimaryKey
    @ColumnInfo(name = "Id")
    var id: Int,

    @ColumnInfo(name = "RecursoNombre")
    val recursoNombre: Int,

    @ColumnInfo(name = "Vida")
    var vida: Int,

    @ColumnInfo(name = "Mente")
    var mente: Int,

    @ColumnInfo(name = "Ataque")
    var ataque: Int,

    @ColumnInfo(name = "Defensa")
    var defensa: Int,
)