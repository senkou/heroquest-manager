package com.senkou.heroquestmanager.data.local

import com.senkou.heroquestmanager.data.room.databases.CampaniasDB
import com.senkou.heroquestmanager.data.room.entity.Clase
import com.senkou.heroquestmanager.data.room.entity.Equipo
import com.senkou.heroquestmanager.data.room.entity.Personaje
import com.senkou.heroquestmanager.domain.funcionalidad.toCampania
import com.senkou.heroquestmanager.domain.funcionalidad.toCampaniaUI
import com.senkou.heroquestmanager.domain.funcionalidad.toEquipo
import com.senkou.heroquestmanager.domain.funcionalidad.toPersonaje
import com.senkou.heroquestmanager.domain.funcionalidad.toPersonajes
import com.senkou.heroquestmanager.domain.model.CampaniaUI

class RoomRepository(private val dataBase: CampaniasDB) : CampaniasService, PersonajesService {

    //region Campanias
    override val campanias = dataBase.campaniaDAO().getAll()

    override suspend fun existsCampania(id: Int) = dataBase.campaniaDAO().getById(id) != null

    override suspend fun saveCampania(campania: CampaniaUI) {
        dataBase.campaniaDAO().insert(campania.toCampania())
        dataBase.personajeDAO().insertAll(campania.personajes.toPersonajes())
    }

    override suspend fun importCampania(campania: CampaniaUI, callback: () -> Unit) {
        dataBase.campaniaDAO().insert(campania.toCampania())
        campania.personajes.forEach { personaje ->
            val newId = dataBase.personajeDAO().insert(personaje.toPersonaje())
            dataBase.equipoDAO().insertAll(personaje.copy(id = newId.toInt()).toEquipo())
        }
        callback()
    }

    override suspend fun exportCampania(idCampania: Int) =
        dataBase.campaniaDAO().getCampaniaToExport(idCampania).toCampaniaUI()

    override suspend fun deleteCampania(id: Int) {
        val campania = dataBase.campaniaDAO().getById(id)
        campania?.let {
            val personajes = dataBase.personajeDAO().getByCampaniaId(campania.id)
            personajes.forEach {
                dataBase.equipoDAO().deleteAll(it.id)
            }
            dataBase.personajeDAO().deleteAllByCampania(campania.id)
            dataBase.campaniaDAO().delete(campania)
        }
    }
    //endregion

    //region Personajes
    override val objetos = dataBase.objetoDAO().getAll()
    override suspend fun getClaseById(idClase: Int): Clase {
        return dataBase.claseDAO().getById(idClase)
    }

    override suspend fun getPersonajeDb(idPersonaje: Int): Personaje {
        return dataBase.personajeDAO().getById(idPersonaje)
    }

    override fun getPersonajes(idCampania: Int) =
        dataBase.personajeDAO().getAllByCampaniaId(idCampania)

    override suspend fun updatePersonaje(personaje: Personaje) {
        dataBase.personajeDAO().update(personaje)
    }

    override suspend fun anadirEquipo(idPersonaje: Int, idObjeto: Int) {
        dataBase.equipoDAO().insert(Equipo(idPersonaje, idObjeto))
    }

    override suspend fun borrarEquipo(idPersonaje: Int, idObjeto: Int) {
        dataBase.equipoDAO().delete(Equipo(idPersonaje, idObjeto))
    }
    //endregion
}