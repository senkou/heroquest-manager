package com.senkou.heroquestmanager.data.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    foreignKeys = [
        ForeignKey(
            entity = Campania::class,
            parentColumns = arrayOf("Id"),
            childColumns = arrayOf("IdCampania"),
            onDelete = ForeignKey.SET_NULL
        ),
        ForeignKey(
            entity = Clase::class,
            parentColumns = arrayOf("Id"),
            childColumns = arrayOf("IdClase"),
            onDelete = ForeignKey.SET_NULL
        )
    ],
    indices = [
        Index("IdCampania"),
        Index("IdClase")
    ]
)

data class Personaje(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "Id")
    val id: Int = 0,

    @ColumnInfo(name = "NombreJugador")
    val nombreJugador: String,

    @ColumnInfo(name = "IdCampania") // FK
    val idCampania: Int,

    @ColumnInfo(name = "IdClase") // FK
    val idClase: Int,

    @ColumnInfo(name = "Sexo")
    val sexo: String,

    @ColumnInfo(name = "Vida")
    var vida: Int,

    @ColumnInfo(name = "Mente")
    var mente: Int,

    @ColumnInfo(name = "Ataque")
    var ataque: Int,

    @ColumnInfo(name = "Defensa")
    var defensa: Int,

    @ColumnInfo(name = "Oro")
    var oro: Int,
)
