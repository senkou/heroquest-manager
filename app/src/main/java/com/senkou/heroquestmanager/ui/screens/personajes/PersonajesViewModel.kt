package com.senkou.heroquestmanager.ui.screens.personajes

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.senkou.heroquestmanager.MyApplication
import com.senkou.heroquestmanager.data.local.PersonajesService
import com.senkou.heroquestmanager.data.room.entity.Clase
import com.senkou.heroquestmanager.data.room.entity.Objeto
import kotlinx.coroutines.flow.last
import kotlinx.coroutines.launch
import kotlin.math.max
import kotlin.math.min

interface IPersonajesViewModel {
    fun anadirEquipo(idPersonaje: Int)
    fun mostrarDetallesObjeto(idResImagen: Int)
    fun mostrarDetallesPersonaje(idClase: Int)
    fun borrarObjeto(idPersonaje: Int, idObjeto: Int, idResString: Int)
    fun hacerTirada(dados: Int)
}

class PersonajesViewModel(
    private val provider: PersonajesService,
    idCampania: Int
) : ViewModel() {

    private val campania = idCampania.takeIf { idCampania > 0 } ?: 0
    val personajes = provider.getPersonajes(campania)
    val objetos = provider.objetos



    var callBackPersonajes: IPersonajesViewModel? = null
    fun hacerTirada(numeroDados: Int) {
        callBackPersonajes?.hacerTirada(numeroDados)
    }

    fun mostrarDetallesPersonaje(idClase: Int) {
        callBackPersonajes?.mostrarDetallesPersonaje(idClase)
    }

    fun anadirEquipo(idPersonaje: Int) {
        callBackPersonajes?.anadirEquipo(idPersonaje)
    }

    fun mostrarDetallesObjeto(idObjeto: Int) {
        viewModelScope.launch {
            val recurso = objetos.last().find { objeto -> objeto.id == idObjeto }?.recursoDrawable
            recurso?.let { res ->
                callBackPersonajes?.mostrarDetallesObjeto(res)
            }
        }
    }

    fun borrarObjeto(idPersonaje: Int, idObjeto: Int) {
        viewModelScope.launch {
            val idResString = objetos.last().find { objeto -> objeto.id == idObjeto }?.recursoNombre
            idResString?.let {
                callBackPersonajes?.borrarObjeto(idPersonaje, idObjeto, idResString)
            }
        }
    }

    fun getObjetos(callback: (List<Objeto>) -> Unit) {
        viewModelScope.launch {
            objetos.collect {
                callback(it)
            }
        }
    }

    fun modificarOro(idPersonaje: Int, cantidad: Int) {
        viewModelScope.launch {
            val personaje = provider.getPersonajeDb(idPersonaje)
            personaje.oro += cantidad
            personaje.oro = max(0, personaje.oro)

            provider.updatePersonaje(personaje)
        }
    }

    fun modificarAtaque(idPersonaje: Int, cantidad: Int) {
        viewModelScope.launch {
            val personaje = provider.getPersonajeDb(idPersonaje)
            personaje.ataque += cantidad
            personaje.ataque = max(0, personaje.ataque)

            provider.updatePersonaje(personaje)
        }
    }

    fun modificarDefensa(idPersonaje: Int, cantidad: Int) {
        viewModelScope.launch {
            val personaje = provider.getPersonajeDb(idPersonaje)
            personaje.defensa += cantidad
            personaje.defensa = max(0, personaje.defensa)

            provider.updatePersonaje(personaje)
        }
    }

    private suspend fun getClase(idClase: Int): Clase {
        return provider.getClaseById(idClase)
    }

    fun modificarCuerpo(idPersonaje: Int, cantidad: Int) {
        viewModelScope.launch {
            val personaje = provider.getPersonajeDb(idPersonaje)
            val vidaMaxima = getClase(personaje.idClase).vida
            personaje.vida += cantidad
            personaje.vida = min(personaje.vida, vidaMaxima)
            personaje.vida = max(0, personaje.vida)

            provider.updatePersonaje(personaje)
        }
    }

    fun modificarMente(idPersonaje: Int, cantidad: Int) {
        viewModelScope.launch {
            val personaje = provider.getPersonajeDb(idPersonaje)
            val menteMaxima = getClase(personaje.idClase).mente
            personaje.mente += cantidad
            personaje.mente = min(personaje.mente, menteMaxima)
            personaje.mente = max(0, personaje.mente)

            provider.updatePersonaje(personaje)
        }
    }

    fun anadirObjetoPersonaje(idPersonaje: Int, idObjeto: Int) {
        viewModelScope.launch {
            provider.anadirEquipo(idPersonaje, idObjeto)
        }
    }

    fun borrarObjetoPersonaje(idPersonaje: Int, idObjeto: Int) {
        viewModelScope.launch {
            provider.borrarEquipo(idPersonaje, idObjeto)
        }
    }

    companion object {
        class PersonajesViewModelFactory(
            private val application: MyApplication,
            private val idCampania: Int = 0
        ) :
            ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                if (modelClass.isAssignableFrom(PersonajesViewModel::class.java)) {
                    @Suppress("UNCHECKED_CAST")
                    return PersonajesViewModel(application.roomProvider, idCampania) as T
                }
                throw IllegalArgumentException("Unknown ViewModel class")
            }
        }
    }
}