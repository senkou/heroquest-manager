package com.senkou.heroquestmanager.ui.popups

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.DialogFragment
import com.senkou.heroquestmanager.databinding.VistaCartaBinding
import com.senkou.heroquestmanager.domain.funcionalidad.playSonidoBoton

class PopDetalle : DialogFragment() {

    private val binding by lazy {
        VistaCartaBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireDialog().window?.setLayout(
            LinearLayout.LayoutParams.WRAP_CONTENT, // Width of popup window
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        requireDialog().window?.setElevation(10.0F)

        val idResImagen = arguments?.getInt("idResImagen")

        idResImagen?.let {
            val drawable = ResourcesCompat.getDrawable(resources, idResImagen, null)
            binding.imDetalleCarta.setImageDrawable(drawable)
        }

        // Set a click listener for popup's button widget
        binding.btSalirDetalle.setOnClickListener {
            playSonidoBoton()
            dismiss()
        }
    }
}