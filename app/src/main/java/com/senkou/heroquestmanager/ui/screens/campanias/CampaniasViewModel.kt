package com.senkou.heroquestmanager.ui.screens.campanias

import android.os.Environment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.senkou.heroquestmanager.MyApplication
import com.senkou.heroquestmanager.data.local.CampaniasService
import com.senkou.heroquestmanager.data.model.Clases
import com.senkou.heroquestmanager.domain.model.CampaniaUI
import com.senkou.heroquestmanager.domain.model.PersonajeCreado
import com.senkou.heroquestmanager.domain.model.PersonajeUI
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File
import java.io.InputStream
import kotlin.random.Random

interface ICampaniasViewModel {
    fun exportadoConExito(exito: Boolean)
}

class CampaniasViewModel(private val provider: CampaniasService) : ViewModel() {

    val campanias = provider.campanias
    var dirs: File? = null
    var callBackCampanias: ICampaniasViewModel? = null

    fun crearCampania(nombreCampania: String, personajesCreados: List<PersonajeCreado>) {

        viewModelScope.launch {

            val personajes: MutableList<PersonajeUI> = mutableListOf()

            personajesCreados.forEach { personajeCreado ->
                val clase = provider.getClaseById(personajeCreado.clase.id)
                personajes.add(
                    PersonajeUI(
                        nombreJugador = personajeCreado.nombre,
                        idClase = clase.id,
                        sexo = personajeCreado.sexo.name,
                        vida = clase.vida,
                        mente = clase.mente,
                        ataque = clase.ataque,
                        defensa = clase.defensa,
                    )
                )
            }

            val campania = CampaniaUI(
                nombre = nombreCampania,
                barbaro = personajesCreados.any { it.clase == Clases.BARBARO },
                enano = personajesCreados.any { it.clase == Clases.ENANO },
                elfo = personajesCreados.any { it.clase == Clases.ELFO },
                mago = personajesCreados.any { it.clase == Clases.MAGO },
                personajes = personajes
            )

            anadirCampania(campania)
        }
    }

    private suspend fun anadirCampania(campania: CampaniaUI) {

        val newId = getNewUniqueId()

        val newCampania = campania.copy(
            id = newId,
            personajes = campania.personajes.map { it.copy(idCampania = newId) }
        )

        provider.saveCampania(newCampania)
    }

    private suspend fun getNewUniqueId(): Int {
        var id: Int
        do {
            id = randomId().toInt()
        } while (provider.existsCampania(id))
        return id
    }

    fun borrarCampania(indice: Int) {
        viewModelScope.launch {
            provider.deleteCampania(indice)
        }
    }

    fun exportCampania(nombreCampania: String, idCampaniaUI: Int) {

        var file: File
        var acumulador = ""
        var indice = 0

        do {
            file = File(
                File(
                    Environment.getExternalStorageDirectory(),
                    Environment.DIRECTORY_DOWNLOADS
                ), "${nombreCampania}(${idCampaniaUI})$acumulador.hqm"
            )
            indice++
            acumulador = " ($indice)"

        } while (file.exists())

        exportar(file, idCampaniaUI)
    }

    private fun exportar(file: File, id: Int) {
        viewModelScope.launch(Dispatchers.IO) {

            val campania = provider.exportCampania(id)
            try {
                val jsonText = Gson().toJson(campania)
                file.writeText(jsonText)
                callBackCampanias?.exportadoConExito(true)
            } catch (e: Exception) {
                callBackCampanias?.exportadoConExito(false)
                e.printStackTrace()
            }
        }
    }


    fun importar(stream: InputStream, importCallback: () -> Unit) {

        val contenidoJson = stream.bufferedReader().readText()
        val campania = if (contenidoJson.isNotEmpty()) {
            Gson().fromJson(contenidoJson, CampaniaUI::class.java)
        } else {
            null
        }
        stream.bufferedReader().close()
        stream.close()

        campania?.let {
            viewModelScope.launch(Dispatchers.IO) {

                val newCampania: CampaniaUI = if (provider.existsCampania(campania.id)) {
                    val newId = getNewUniqueId()
                    campania.copy(
                        id = newId,
                        personajes = campania.personajes.map { it.copy(idCampania = newId) })
                } else {
                    campania
                }

                provider.importCampania(newCampania) {
                    launch(Dispatchers.Main) {
                        importCallback()
                    }
                }
            }
        }
    }

    private fun randomId(): Long {
        return Random.nextLong(0, 9999999)
    }

    companion object {
        class CampaniasViewModelFactory(private val application: MyApplication) :
            ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                if (modelClass.isAssignableFrom(CampaniasViewModel::class.java)) {
                    @Suppress("UNCHECKED_CAST")
                    return CampaniasViewModel(application.roomProvider) as T
                }
                throw IllegalArgumentException("Unknown ViewModel class")
            }
        }
    }
}


