package com.senkou.heroquestmanager.ui.popups

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.senkou.heroquestmanager.R
import com.senkou.heroquestmanager.databinding.PopTiradaBinding
import com.senkou.heroquestmanager.domain.model.TiradaDado
import com.senkou.heroquestmanager.ui.screens.personajes.ITiradaAdapter
import com.senkou.heroquestmanager.ui.screens.personajes.TiradaAdapter


class PopTirada : DialogFragment() {

    private val binding by lazy {
        PopTiradaBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireDialog().window?.setLayout(
            ConstraintLayout.LayoutParams.MATCH_PARENT,
            ConstraintLayout.LayoutParams.WRAP_CONTENT
        )
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        requireDialog().window?.setElevation(10.0F)

        val tiradas = arguments?.getInt("tiradas", 0)

        val tiradasDados = ArrayList<TiradaDado>()

        tiradas?.let {
            (1..tiradas).forEach { _ ->
                val nuevaTirada = obtenerTirada()
                getDrawable(nuevaTirada)?.let { cara ->
                    tiradasDados.add(TiradaDado(nuevaTirada, cara))
                }
            }
        }

        if (tiradasDados.isNotEmpty()) {
            binding.rvDados.adapter = TiradaAdapter(tiradasDados, object : ITiradaAdapter {
                override fun volverATirar(indice: Int) {
                    val nuevaTirada = obtenerTirada()
                    tiradasDados[indice].valor = nuevaTirada
                    val nuevaCara = getDrawable(nuevaTirada)
                    tiradasDados[indice].imagen = nuevaCara!!
                    binding.rvDados.adapter?.notifyItemChanged(indice)
                }
            })
        } else {
            dismiss()
        }
    }

    // 1 y 6 escudo negro y calavera
    // 2 calavera y 5 escudo blanco
    // 3 calavera y 4 escudo blanco
    private fun getDrawable(id: Int): Drawable? = when (id) {
        2, 3, 6 -> ContextCompat.getDrawable(requireContext(), R.drawable.calavera_hq)
        4, 5 -> ContextCompat.getDrawable(requireContext(), R.drawable.escudo_blanco_hq)
        1 -> ContextCompat.getDrawable(requireContext(), R.drawable.escudo_negro_hq)
        else -> null
    }

    private fun obtenerTirada(): Int = (1..6).random()
}