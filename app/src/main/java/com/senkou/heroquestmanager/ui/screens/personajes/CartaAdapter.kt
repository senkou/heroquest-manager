package com.senkou.heroquestmanager.ui.screens.personajes

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.senkou.heroquestmanager.data.room.entity.Objeto
import com.senkou.heroquestmanager.databinding.CardCartaBinding
import com.senkou.heroquestmanager.domain.funcionalidad.playSonidoBoton


class CartaAdapter(
    private val addCardCallback: (Int) -> Unit
) : ListAdapter<Objeto, CartaAdapter.ViewHolder>(ObjetoComparator()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            CardCartaBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val objeto = currentList[position]

        val drawable = ResourcesCompat.getDrawable(
            holder.itemView.context.resources,
            objeto.recursoDrawable,
            null
        )
        holder.imagenCarta.setImageDrawable(drawable)
        holder.imagenCarta.setOnClickListener {
            playSonidoBoton()
            addCardCallback(objeto.id)
        }
    }


    class ViewHolder(binding: CardCartaBinding) : RecyclerView.ViewHolder(binding.root) {
        val imagenCarta: ImageView = binding.imgCartaObjeto
    }

    class ObjetoComparator : DiffUtil.ItemCallback<Objeto>() {
        override fun areItemsTheSame(oldItem: Objeto, newItem: Objeto): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Objeto, newItem: Objeto): Boolean =
            oldItem == newItem

    }
}