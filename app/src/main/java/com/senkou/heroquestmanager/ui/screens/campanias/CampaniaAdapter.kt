package com.senkou.heroquestmanager.ui.screens.campanias

import android.graphics.Color
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.senkou.heroquestmanager.databinding.CardCampaniaBinding
import com.senkou.heroquestmanager.domain.funcionalidad.playSonidoBoton
import com.senkou.heroquestmanager.domain.model.CampaniaUI


interface ICampaniaAdapter {
    fun seleccionarCampania(idCampaniaUI: Int)
    fun exportarCampania(nombreCampania: String, idCampaniaUI: Int)
    fun borrarCampania(idCampaniaUI: Int)
}

class CampaniaAdapter(
    private val color: Int,
    private val callBack: ICampaniaAdapter
) : ListAdapter<CampaniaUI, CampaniaAdapter.ViewHolder>(CampaniasUIComparator()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            CardCampaniaBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = currentList[position]
        holder.bind(item.id, callBack)

        holder.nombre.text = item.nombre

//         si un personaje no está, se deja su imagen atenuada
        if (!item.barbaro) {
            holder.imgBarbaro.drawable.setTint(color)
            holder.imgBarbaro.drawable.setTintMode(PorterDuff.Mode.DST_OUT)
        } else {
            holder.imgBarbaro.drawable.setTint(Color.TRANSPARENT)
        }

        if (!item.enano) {
            holder.imgEnano.drawable.setTint(color)
            holder.imgEnano.drawable.setTintMode(PorterDuff.Mode.DST_OUT)
        } else {
            holder.imgEnano.drawable.setTint(Color.TRANSPARENT)
        }

        if (!item.elfo) {
            holder.imgElfo.drawable.setTint(color)
            holder.imgElfo.drawable.setTintMode(PorterDuff.Mode.DST_OUT)
        } else {
            holder.imgElfo.drawable.setTint(Color.TRANSPARENT)
        }

        if (!item.mago) {
            holder.imgMago.drawable.setTint(color)
            holder.imgMago.drawable.setTintMode(PorterDuff.Mode.DST_OUT)
        } else {
            holder.imgMago.drawable.setTint(Color.TRANSPARENT)
        }

        holder.btExportar.setOnClickListener {
            playSonidoBoton()
            callBack.exportarCampania(item.nombre, item.id)
        }

        holder.btBorrar.setOnClickListener {
            playSonidoBoton()
            callBack.borrarCampania(item.id)
        }
    }

    class ViewHolder(private val binding: CardCampaniaBinding) :
        RecyclerView.ViewHolder(binding.root) {
        //        val jugadas: TextView = binding.txJugadas
        val nombre: TextView = binding.txNombreCampania
        val imgBarbaro: ImageView = binding.imPj1
        val imgEnano: ImageView = binding.imPj2
        val imgElfo: ImageView = binding.imPj3
        val imgMago: ImageView = binding.imPj4
        val btExportar: ImageButton = binding.btExportar
        val btBorrar: ImageButton = binding.btBorrar

        fun bind(idCampania: Int, callBack: ICampaniaAdapter) {
            binding.root.setOnClickListener {
                playSonidoBoton()
                callBack.seleccionarCampania(idCampania)
            }

//            binding.root.setOnLongClickListener {
//                playSonidoBoton()
//                callBack.borrarCampania(idCampania)
//                true
//            }
        }
    }

    class CampaniasUIComparator : DiffUtil.ItemCallback<CampaniaUI>() {
        override fun areItemsTheSame(oldItem: CampaniaUI, newItem: CampaniaUI) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: CampaniaUI, newItem: CampaniaUI): Boolean {
            return oldItem == newItem
        }

    }
}