package com.senkou.heroquestmanager.ui.popups

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import androidx.appcompat.widget.SwitchCompat
import androidx.fragment.app.DialogFragment
import com.senkou.heroquestmanager.data.model.Clases
import com.senkou.heroquestmanager.data.model.Sexo
import com.senkou.heroquestmanager.databinding.PopCrearBinding
import com.senkou.heroquestmanager.domain.funcionalidad.playSonidoBoton
import com.senkou.heroquestmanager.domain.model.PersonajeCreado

class PopCrear(
    private val callbackCrear: (nombre: String, personajes: List<PersonajeCreado>) -> Unit
) : DialogFragment() {

    private val binding by lazy {
        PopCrearBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding.swBarbaro.setOnClickListener {
            binding.edJugadorBarbaro.isEnabled = binding.swBarbaro.isChecked
        }
        binding.swEnano.setOnClickListener {
            binding.edJugadorEnano.isEnabled = binding.swEnano.isChecked
        }
        binding.swElfo.setOnClickListener {
            binding.edJugadorElfo.isEnabled = binding.swElfo.isChecked
        }
        binding.swMago.setOnClickListener {
            binding.edJugadorMago.isEnabled = binding.swMago.isChecked
        }

        binding.btnCancelar.setOnClickListener {
            playSonidoBoton()
            dismiss()
        }

        binding.btnAceptar.setOnClickListener {
            playSonidoBoton()

            val personajesCreados = comprobarPersonajesCreados()

            callbackCrear(
                binding.edNombreCampania.text.toString().trim(),
                personajesCreados
            )

            // Dismiss the popup window
            dismiss()
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireDialog().window?.setElevation(10.0F)
        requireDialog().window?.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT, // Width of popup window
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    private fun comprobarPersonajesCreados(): List<PersonajeCreado> =
        comprobarPersonbaje(binding.swBarbaro, binding.edJugadorBarbaro, Clases.BARBARO) +
                comprobarPersonbaje(binding.swEnano, binding.edJugadorEnano, Clases.ENANO) +
                comprobarPersonbaje(binding.swElfo, binding.edJugadorElfo, Clases.ELFO) +
                comprobarPersonbaje(binding.swMago, binding.edJugadorMago, Clases.MAGO)

    private fun comprobarPersonbaje(switch: SwitchCompat, textoNombre: EditText, clase: Clases) =
        if (switch.isChecked) {
            listOf(
                PersonajeCreado(
                    textoNombre.text.toString().trim(),
                    clase,
                    Sexo.HOMBRE
                )
            )
        } else {
            listOf()
        }
}
