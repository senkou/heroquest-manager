package com.senkou.heroquestmanager.ui.screens.personajes

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.senkou.heroquestmanager.R
import com.senkou.heroquestmanager.databinding.CardPersonajeBinding
import com.senkou.heroquestmanager.domain.funcionalidad.playSonidoBoton
import com.senkou.heroquestmanager.domain.model.PersonajeUI

interface IPersonajes {
    fun modificarCuerpo(idPersonaje: Int, valor: Int)
    fun modificarMente(idPersonaje: Int, valor: Int)
    fun modificarAtaque(idPersonaje: Int, valor: Int)
    fun modificarDefensa(idPersonaje: Int, valor: Int)
    fun modificarOro(idPersonaje: Int, sumar: Boolean)
    fun aniadirEquipo(idPersonaje: Int)
    fun mostrarDetallesPersonaje(idClase: Int)
    fun mostrarDetalleObjeto(idObjeto: Int)
    fun borrarObjeto(idPersonaje: Int, idObjeto: Int)
    fun hacerTirada(dados: Int)
}

class PersonajesAdapter(
    private val context: Context,
    private val callback: IPersonajes
) : ListAdapter<PersonajeUI, PersonajesAdapter.ViewHolder>(PersonajesComparator()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            CardPersonajeBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val personaje = currentList[position]

        holder.nombre.text = personaje.nombreJugador

        holder.imgClase.setOnClickListener {
            playSonidoBoton()
            callback.mostrarDetallesPersonaje(personaje.idClase)
        }
        when (personaje.idClase) {
            1 ->
                holder.imgClase.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.pj_barbaro_grande
                    )
                )

            2 ->
                holder.imgClase.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.pj_enano_grande
                    )
                )

            3 ->
                holder.imgClase.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.pj_elfo_grande
                    )
                )

            4 ->
                holder.imgClase.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.pj_mago_grande
                    )
                )
        }

        holder.cuerpo.text = personaje.vida.toString()
        holder.mente.text = personaje.mente.toString()
        holder.ataque.text = personaje.ataque.toString()
        holder.ataque.setOnClickListener {
            playSonidoBoton()
            callback.hacerTirada(personaje.ataque)
        }
        holder.defensa.text = personaje.defensa.toString()
        holder.defensa.setOnClickListener {
            playSonidoBoton()
            callback.hacerTirada(personaje.defensa)
        }
        holder.oro.text = personaje.oro.toString()

        val adapter = EquipoAdapter(context, callback)
        adapter.submitList(personaje.equipo)
        holder.ltObjetos.adapter = adapter

        holder.btVidaMas.setOnClickListener {
            playSonidoBoton()
            callback.modificarCuerpo(personaje.id, 1)
        }

        holder.btVidaMenos.setOnClickListener {
            playSonidoBoton()
            callback.modificarCuerpo(personaje.id, -1)
        }

        holder.btMenteMas.setOnClickListener {
            playSonidoBoton()
            callback.modificarMente(personaje.id, 1)
        }

        holder.btMenteMenos.setOnClickListener {
            playSonidoBoton()
            callback.modificarMente(personaje.id, -1)
        }

        holder.btAtaqueMas.setOnClickListener {
            playSonidoBoton()
            callback.modificarAtaque(personaje.id, 1)
        }

        holder.btAtaqueMenos.setOnClickListener {
            playSonidoBoton()
            callback.modificarAtaque(personaje.id, -1)
        }


        holder.btDefensaMas.setOnClickListener {
            playSonidoBoton()
            callback.modificarDefensa(personaje.id, 1)
        }

        holder.btDefensaMenos.setOnClickListener {
            playSonidoBoton()
            callback.modificarDefensa(personaje.id, -1)
        }

        holder.ltObjetos.setOnTouchListener { v, event ->

            when (event.action) {
                MotionEvent.ACTION_DOWN ->                 // Disallow ScrollView to intercept touch events.
                    v.parent.requestDisallowInterceptTouchEvent(true)

                MotionEvent.ACTION_UP ->                 // Allow ScrollView to intercept touch events.
                    v.parent.requestDisallowInterceptTouchEvent(false)
            }

            // Handle ListView touch events.
            v.onTouchEvent(event)
            true
        }

        holder.btOroMas.setOnClickListener {
            playSonidoBoton()
            callback.modificarOro(personaje.id, true)
        }

        holder.btOroMenos.setOnClickListener {
            playSonidoBoton()
            callback.modificarOro(personaje.id, false)
        }

        holder.btAnadirEquipo.setOnClickListener {
            playSonidoBoton()
            callback.aniadirEquipo(personaje.id)
        }
    }

    // Asignamos la relación entre los datos y la vista
    class ViewHolder(binding: CardPersonajeBinding) : RecyclerView.ViewHolder(binding.root) {
        val nombre = binding.edNombrePj
        val imgClase = binding.imClase
        val cuerpo = binding.txPv
        val mente = binding.txPm
        val ataque = binding.txDadosAtaque
        val defensa = binding.txDadosDefensa
        val oro = binding.txOro
        val btVidaMas = binding.btVidaMas
        val btVidaMenos = binding.btVidaMenos
        val btMenteMas = binding.btMenteMas
        val btMenteMenos = binding.btMenteMenos
        val btAtaqueMas = binding.btAtaqueMas
        val btAtaqueMenos = binding.btAtaqueMenos
        val btDefensaMas = binding.btDefensaMas
        val btDefensaMenos = binding.btDefensaMenos
        val btOroMas = binding.btOroMas
        val btOroMenos = binding.btOroMenos
        val ltObjetos = binding.ltObjetos
        val btAnadirEquipo = binding.btAnadirEquipo
    }

    class PersonajesComparator : DiffUtil.ItemCallback<PersonajeUI>() {
        override fun areItemsTheSame(oldItem: PersonajeUI, newItem: PersonajeUI) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: PersonajeUI, newItem: PersonajeUI) =
            oldItem == newItem

    }
}