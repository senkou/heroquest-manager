package com.senkou.heroquestmanager.ui.popups

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.DialogFragment
import com.senkou.heroquestmanager.R
import com.senkou.heroquestmanager.databinding.PopOroBinding
import com.senkou.heroquestmanager.domain.funcionalidad.orTrue
import com.senkou.heroquestmanager.domain.funcionalidad.playSonidoBoton

class PopOro(private val callbaclModificarOro: (idPersonaje: Int, cantidad: Int) -> Unit) :
    DialogFragment() {

    private val binding by lazy {
        PopOroBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireDialog().window?.setLayout(
            LinearLayout.LayoutParams.WRAP_CONTENT, // Width of popup window
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        requireDialog().window?.setElevation(10.0F)

        val sumar = arguments?.getBoolean("isSuma").orTrue()
        val idPersonaje = arguments?.getInt("idPersonaje")

        binding.txTituloOro.text = if (sumar) {
            requireContext().getString(R.string.tx_oro_mas)
        } else {
            requireContext().getString(R.string.tx_oro_menos)
        }

        binding.btOroCancelar.setOnClickListener {
            playSonidoBoton()
            dismiss()
        }

        binding.btOroAceptar.setOnClickListener {
            playSonidoBoton()
            var cantidad = binding.edOro.text.toString().toInt()

            if (!sumar) {
                cantidad *= -1
            }

            idPersonaje?.let {
                callbaclModificarOro(idPersonaje, cantidad)
            }

            dismiss()
        }
    }
}


