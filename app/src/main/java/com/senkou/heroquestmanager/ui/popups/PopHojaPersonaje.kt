package com.senkou.heroquestmanager.ui.popups

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.senkou.heroquestmanager.R
import com.senkou.heroquestmanager.databinding.PopHojaPersonajeBinding

class PopHojaPersonaje : DialogFragment() {

    private val binding by lazy {
        PopHojaPersonajeBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireDialog().window?.setLayout(
            LinearLayout.LayoutParams.WRAP_CONTENT, // Width of popup window
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        requireDialog().window?.setElevation(10.0F)

        val idClase = arguments?.getInt("clase", 0)

        val resDrawable = getDrawableId(idClase ?: 0)

        resDrawable?.let {
            binding.imgHojaPersonaje.setImageDrawable(
                ContextCompat.getDrawable(
                    requireContext(),
                    resDrawable
                )
            )
        }
    }

    private fun getDrawableId(id: Int) = when (id) {
        1 -> R.drawable.hoja_barbaro
        2 -> R.drawable.hoja_enano
        3 -> R.drawable.hoja_elfo
        4 -> R.drawable.hoja_mago
        else -> {
            null
        }
    }
}