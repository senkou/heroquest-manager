package com.senkou.heroquestmanager.ui

import android.media.AudioManager
import android.media.SoundPool
import android.os.Bundle
import android.view.WindowManager
import android.widget.Toast
import androidx.activity.addCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.senkou.heroquestmanager.R
import com.senkou.heroquestmanager.databinding.ActivityMainBinding

val sp: SoundPool = SoundPool.Builder().setMaxStreams(8).build()
var soundId: Int? = null

class MainActivity : AppCompatActivity() {
    private var presionado: Long = 0

    private val binding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        soundId = sp.load(this, R.raw.click_01, 1)
        this.volumeControlStream = AudioManager.STREAM_MUSIC

        onBackPressedDispatcher.addCallback {
            if (onBackPressedDispatcher.hasEnabledCallbacks()) {
                findNavController(R.id.nav_host_fragment).navigateUp()
            } else {
                if (presionado + 1000 > System.currentTimeMillis()) {
                    super.onBackPressed()
                } else {
                    Toast.makeText(
                        this@MainActivity,
                        "Pulsa dos veces para salir",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                presionado = System.currentTimeMillis()
            }
        }
    }
}