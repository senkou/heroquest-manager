package com.senkou.heroquestmanager.ui.popups

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.DialogFragment
import com.senkou.heroquestmanager.data.room.entity.Objeto
import com.senkou.heroquestmanager.databinding.PopElegirObjetoBinding
import com.senkou.heroquestmanager.domain.funcionalidad.playSonidoBoton
import com.senkou.heroquestmanager.ui.screens.personajes.CartaAdapter

class PopCartas(
    private val listaObjetos: List<Objeto>,
    private val calbackAnadirEquipo: (idObjeto: Int) -> Unit
) :
    DialogFragment() {

    private val binding by lazy {
        PopElegirObjetoBinding.inflate(layoutInflater)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireDialog().window?.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT, // Width of popup window
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        requireDialog().window?.setElevation(10.0F)

        val adapter = CartaAdapter { idObjeto ->
            calbackAnadirEquipo(idObjeto)
            dismiss()
        }

        binding.rvListaObjetos.adapter = adapter

        adapter.submitList(listaObjetos)


        binding.btSalirListaObjetos.setOnClickListener {
            playSonidoBoton()
            dismiss()
        }
    }
}