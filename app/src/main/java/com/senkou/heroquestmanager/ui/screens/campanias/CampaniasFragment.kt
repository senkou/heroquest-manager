package com.senkou.heroquestmanager.ui.screens.campanias

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.findNavController
import com.google.android.material.snackbar.Snackbar
import com.senkou.heroquestmanager.MyApplication
import com.senkou.heroquestmanager.R
import com.senkou.heroquestmanager.databinding.FragmentCampaniasBinding
import com.senkou.heroquestmanager.domain.funcionalidad.playSonidoBoton
import com.senkou.heroquestmanager.domain.funcionalidad.toCampaniaUI
import com.senkou.heroquestmanager.ui.popups.PopCrear
import com.senkou.heroquestmanager.ui.popups.PopPregunta
import com.senkou.heroquestmanager.ui.screens.campanias.CampaniasViewModel.Companion.CampaniasViewModelFactory
import kotlinx.coroutines.launch
import java.io.File

class CampaniasFragment : Fragment() {

    private val binding by lazy {
        FragmentCampaniasBinding.inflate(layoutInflater)
    }

    private val campaniasViewModel: CampaniasViewModel by viewModels {
        CampaniasViewModelFactory(requireActivity().application as MyApplication)
    }

    companion object {
        private const val REQUEST_EXTERNAL_STORAGE = 1
        private const val REQUEST_OPEN = 2
//        private const val REQUEST_SAVE = 3
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter: CampaniaAdapter?

        campaniasViewModel.dirs = requireContext().getExternalFilesDir(null)

        campaniasViewModel.callBackCampanias = object : ICampaniasViewModel {
            override fun exportadoConExito(exito: Boolean) {
                val mensaje = if (exito) {
                    requireContext().getString(R.string.export_result_OK)
                } else {
                    requireContext().getString(R.string.export_result_KO)
                }
                Snackbar.make(requireView(), mensaje, Snackbar.LENGTH_SHORT).show()
            }
        }

        // asignamos el adapter para la lista de campañas y lo ponemos a dos columnas
        val iCampaniaAdapter = object : ICampaniaAdapter {
            override fun seleccionarCampania(idCampaniaUI: Int) {
                selectCampania(idCampaniaUI)
            }

            override fun exportarCampania(nombreCampania: String, idCampaniaUI: Int) {
                campaniasViewModel.exportCampania(nombreCampania, idCampaniaUI)
            }

            override fun borrarCampania(idCampaniaUI: Int) {
                deleteCampania(idCampaniaUI)
            }
        }

        adapter = CampaniaAdapter(
            ContextCompat.getColor(requireContext(), R.color.tinte_gris), iCampaniaAdapter
        )

        binding.listaCampanias.adapter = adapter


        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                // varios launch para hacer el collenct en paralelo si hubiera varios flows
                launch {
                    campaniasViewModel.campanias.collect { campanias ->
                        adapter.submitList(campanias.map { it.toCampaniaUI() }.toMutableList())
                    }
                }
            }
        }

        binding.fapCampanias.setOnClickListener { createCampania() }

        binding.fapImportar.setOnClickListener { importCampanias() }
    }

    private fun createCampania() {
        playSonidoBoton()

        // creación de la ventana popup de nueva campaña
        val popCrear = PopCrear { nombre, personajes ->
            campaniasViewModel.crearCampania(nombre, personajes)
        }
        popCrear.show(childFragmentManager, "crear")
    }

    private fun selectCampania(idCampaniaUI: Int) {
        val action =
            CampaniasFragmentDirections.actionCampaniasFragmentToPersonajesFragment(idCampania = idCampaniaUI)
        requireView().findNavController().navigate(action)
    }

    private fun deleteCampania(idCampaniaUI: Int) {
        val popPregunta = PopPregunta {
            campaniasViewModel.borrarCampania(idCampaniaUI)
        }

        val bundle = Bundle()
        bundle.putString("titulo", requireContext().getString(R.string.titulo_alert))
        bundle.putString("mensaje", requireContext().getString(R.string.mensaje_alert))
        popPregunta.arguments = bundle
        popPregunta.show(childFragmentManager, "borrar")
    }


    private fun importCampanias() {
        playSonidoBoton()

        when {
            ContextCompat.checkSelfPermission(
                requireContext(), android.Manifest.permission.READ_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED -> {
                abrirExploradorLeer()
            }

            shouldShowRequestPermissionRationale(android.Manifest.permission.READ_EXTERNAL_STORAGE) -> {
                solicitarPermisosLectura()
            }

            else -> {
                solicitarPermisosLectura()
            }
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        when (requestCode) {
            REQUEST_EXTERNAL_STORAGE -> {
                if (!isExternalStorageWritable() && !isExternalStorageReadable()) {
                    Toast.makeText(
                        requireContext(),
                        requireContext().resources.getString(R.string.almacenamiento_bloqueado),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            REQUEST_OPEN -> {
                data?.let { intent ->
                    intent.data?.let { uri ->
                        val stream = requireContext().contentResolver.openInputStream(uri)
                        stream?.let {
                            campaniasViewModel.importar(stream) {
                                Toast.makeText(
                                    requireContext(),
                                    requireContext().resources.getString(R.string.import_result_OK),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    }
                }
            }
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>, grantResults: IntArray
    ) {
        if (permissions.contains(android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
            abrirExploradorLeer()
        }
    }

    private fun abrirExploradorLeer() {
        if (isExternalStorageReadable()) {

            val startDir = FileProvider.getUriForFile(
                requireContext(),
                requireContext().applicationContext.packageName + ".provider",
                File(Environment.getExternalStorageDirectory(), Environment.DIRECTORY_DOWNLOADS)
            )

            val intent = Intent(Intent.ACTION_GET_CONTENT).apply {
                setDataAndType(startDir, "application/*")
                addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            }
            startActivityForResult(intent, REQUEST_OPEN)
        }
    }

    private fun solicitarPermisosLectura() {
        requestPermissions(
            arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE), REQUEST_EXTERNAL_STORAGE
        )
    }

    /* Checks if external storage is available for read and write */
    private fun isExternalStorageWritable(): Boolean {
        return Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED
    }

    /* Checks if external storage is available to at least read */
    private fun isExternalStorageReadable(): Boolean {
        return Environment.getExternalStorageState() in setOf(
            Environment.MEDIA_MOUNTED, Environment.MEDIA_MOUNTED_READ_ONLY
        )
    }
}