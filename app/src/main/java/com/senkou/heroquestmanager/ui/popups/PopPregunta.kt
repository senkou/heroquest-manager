package com.senkou.heroquestmanager.ui.popups

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.DialogFragment
import com.senkou.heroquestmanager.databinding.PreguntaSiNoBinding
import com.senkou.heroquestmanager.domain.funcionalidad.playSonidoBoton

class PopPregunta(private val callbackAceptar: () -> Unit) : DialogFragment() {

    private val binding by lazy {
        PreguntaSiNoBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireDialog().window?.setLayout(
            LinearLayout.LayoutParams.WRAP_CONTENT, // Width of popup window
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        requireDialog().window?.setElevation(10.0F)

        arguments?.let {
            binding.txTituloPregunta.text = it.getString("titulo", "")
            binding.txDescripcionPregunta.text = it.getString("mensaje", "")
        }
        binding.btPreguntaCancelar.setOnClickListener {
            playSonidoBoton()
            dismiss()
        }

        binding.btPreguntaAceptar.setOnClickListener {
            playSonidoBoton()
            callbackAceptar()
            dismiss()
        }

    }
}