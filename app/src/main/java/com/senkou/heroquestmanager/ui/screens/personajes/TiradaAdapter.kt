package com.senkou.heroquestmanager.ui.screens.personajes

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.senkou.heroquestmanager.databinding.ItemDadoBinding
import com.senkou.heroquestmanager.domain.funcionalidad.playSonidoBoton
import com.senkou.heroquestmanager.domain.model.TiradaDado

interface ITiradaAdapter {
    fun volverATirar(indice: Int)
}

class TiradaAdapter(
    private val tiradasDados: ArrayList<TiradaDado>,
    private val callback: ITiradaAdapter
) : RecyclerView.Adapter<TiradaAdapter.ViewHolder>() {

    class ViewHolder(binding: ItemDadoBinding) : RecyclerView.ViewHolder(binding.root) {
        val imgCaraDado: ImageView = binding.imgCaraDado
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemDadoBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.imgCaraDado.setImageDrawable(tiradasDados[position].imagen)
        holder.imgCaraDado.setOnClickListener {
            playSonidoBoton()
            callback.volverATirar(position)
        }
    }

    override fun getItemCount() = tiradasDados.size
}