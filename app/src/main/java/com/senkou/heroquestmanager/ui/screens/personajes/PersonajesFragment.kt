package com.senkou.heroquestmanager.ui.screens.personajes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.SimpleItemAnimator
import com.senkou.heroquestmanager.MyApplication
import com.senkou.heroquestmanager.R
import com.senkou.heroquestmanager.databinding.FragmentPersonajesBinding
import com.senkou.heroquestmanager.domain.funcionalidad.playSonidoBoton
import com.senkou.heroquestmanager.domain.funcionalidad.toPersonajeUI
import com.senkou.heroquestmanager.ui.popups.PopCartas
import com.senkou.heroquestmanager.ui.popups.PopDetalle
import com.senkou.heroquestmanager.ui.popups.PopHojaPersonaje
import com.senkou.heroquestmanager.ui.popups.PopOro
import com.senkou.heroquestmanager.ui.popups.PopPregunta
import com.senkou.heroquestmanager.ui.popups.PopTirada
import kotlinx.coroutines.launch

class PersonajesFragment : Fragment() {

    private val binding by lazy {
        FragmentPersonajesBinding.inflate(layoutInflater)
    }

    private val args: PersonajesFragmentArgs by navArgs()
    private val idCampania by lazy { args.idCampania }

    private val personajesViewModel: PersonajesViewModel by viewModels {
        PersonajesViewModel.Companion.PersonajesViewModelFactory(
            requireActivity().application as MyApplication,
            idCampania
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter: PersonajesAdapter?

//        personajesViewModel.dirs = requireContext().getExternalFilesDir(null)

        personajesViewModel.callBackPersonajes = object : IPersonajesViewModel {
            override fun anadirEquipo(idPersonaje: Int) {
                addEquipo(idPersonaje)
            }

            override fun mostrarDetallesObjeto(idResImagen: Int) {
                detallesObjeto(idResImagen)
            }

            override fun mostrarDetallesPersonaje(idClase: Int) {
                detallesPersonaje(idClase)
            }

            override fun borrarObjeto(idPersonaje: Int, idObjeto: Int, idResString: Int) {
                deleteObjeto(idPersonaje, idObjeto, idResString)
            }

            override fun hacerTirada(dados: Int) {
                tirada(dados)
            }
        }

        adapter = PersonajesAdapter(requireContext(), object : IPersonajes {
            override fun modificarCuerpo(idPersonaje: Int, valor: Int) {
                personajesViewModel.modificarCuerpo(idPersonaje, valor)
            }

            override fun modificarMente(idPersonaje: Int, valor: Int) {
                personajesViewModel.modificarMente(idPersonaje, valor)
            }

            override fun modificarAtaque(idPersonaje: Int, valor: Int) {
                personajesViewModel.modificarAtaque(idPersonaje, valor)
            }

            override fun modificarDefensa(idPersonaje: Int, valor: Int) {
                personajesViewModel.modificarDefensa(idPersonaje, valor)
            }

            override fun modificarOro(idPersonaje: Int, sumar: Boolean) {
                modifOro(idPersonaje, sumar)
            }

            override fun aniadirEquipo(idPersonaje: Int) {
                personajesViewModel.anadirEquipo(idPersonaje)
            }

            override fun mostrarDetallesPersonaje(idClase: Int) {
                personajesViewModel.mostrarDetallesPersonaje(idClase)
            }

            override fun mostrarDetalleObjeto(idObjeto: Int) {
                personajesViewModel.mostrarDetallesObjeto(idObjeto)
            }

            override fun borrarObjeto(idPersonaje: Int, idObjeto: Int) {
                personajesViewModel.borrarObjeto(idPersonaje, idObjeto)
            }

            override fun hacerTirada(dados: Int) {
                personajesViewModel.hacerTirada(dados)
            }

        })

        binding.listaPersonajes.adapter = adapter

        (binding.listaPersonajes.itemAnimator as SimpleItemAnimator).supportsChangeAnimations =
            false

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch {
                    personajesViewModel.personajes.collect { lista ->
                        val personajes = lista.map { personaje ->
                            personaje.key.toPersonajeUI(
                                personaje.value
                            )
                        }
                        adapter.submitList(personajes)
                    }
                }
            }
        }

        binding.fapVolver.setOnClickListener {
            playSonidoBoton()
            findNavController().navigateUp()
        }
    }

    private fun addEquipo(idPersonaje: Int) {
        personajesViewModel.getObjetos {
            val popCartas = PopCartas(it) { idObjeto ->
                personajesViewModel.anadirObjetoPersonaje(idPersonaje, idObjeto)
            }

            popCartas.show(childFragmentManager, "cartas")
        }
    }

    private fun deleteObjeto(idPersonaje: Int, idObjeto: Int, idResString: Int) {
        val popPregunta = PopPregunta {
            personajesViewModel.borrarObjetoPersonaje(idPersonaje, idObjeto)
        }

        val nombreObjeto = resources.getString(idResString)

        val bundle = Bundle()
        bundle.putString("titulo", requireContext().getString(R.string.titulo_alert))
        bundle.putString(
            "mensaje",
            requireContext().getString(R.string.pregunta_borrar_equipo, nombreObjeto)
        )
        popPregunta.arguments = bundle
        popPregunta.show(childFragmentManager, "borrarObjeto")
    }

    private fun detallesObjeto(idResImagen: Int) {
        // Mostramos el detalle del equipo seleccionado
        val popDetalle = PopDetalle()

        val bundle = Bundle()
        bundle.putInt("idResImagen", idResImagen)
        popDetalle.arguments = bundle
        popDetalle.show(childFragmentManager, "detallesObjeto")
    }

    private fun detallesPersonaje(idClase: Int) {
        val popHojaPersonaje = PopHojaPersonaje()

        val bundle = Bundle()
        bundle.putInt("clase", idClase)
        popHojaPersonaje.arguments = bundle
        popHojaPersonaje.show(childFragmentManager, "detallesPersonaje")
    }

    private fun modifOro(idPersonaje: Int, sumar: Boolean) {
        val popOro = PopOro { idPj, cantidad ->
            personajesViewModel.modificarOro(idPj, cantidad)
        }

        val bundle = Bundle()
        bundle.putString(
            "pregunta", if (sumar) {
                requireContext().getString(R.string.tx_oro_mas)
            } else {
                requireContext().getString(R.string.tx_oro_menos)
            }
        )
        bundle.putBoolean("isSuma", sumar)
        bundle.putInt("idPersonaje", idPersonaje)

        popOro.arguments = bundle

        popOro.show(childFragmentManager, "oro")
    }

    private fun tirada(dados: Int) {
        val popTirada = PopTirada()
        val arguments = Bundle()
        arguments.putInt("tiradas", dados)
        popTirada.arguments = arguments
        popTirada.show(childFragmentManager, "tiradaDados")
    }
}