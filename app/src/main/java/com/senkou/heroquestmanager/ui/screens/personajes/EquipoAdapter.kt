package com.senkou.heroquestmanager.ui.screens.personajes

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.senkou.heroquestmanager.databinding.EquipoItemBinding
import com.senkou.heroquestmanager.domain.funcionalidad.playSonidoBoton
import com.senkou.heroquestmanager.domain.model.ObjetoEquipoUI

class EquipoAdapter(private val context: Context, private val callback: IPersonajes) :
    ListAdapter<ObjetoEquipoUI, EquipoAdapter.ViewHolder>(EquipoComparator()) {

    class ViewHolder(binding: EquipoItemBinding) : RecyclerView.ViewHolder(binding.root) {
        val nombreObjeto: TextView = binding.nombreObjeto
        val btnBorrar: ImageButton = binding.btnBorrar
    }

    class EquipoComparator : DiffUtil.ItemCallback<ObjetoEquipoUI>() {
        override fun areItemsTheSame(oldItem: ObjetoEquipoUI, newItem: ObjetoEquipoUI) =
            oldItem.idObjeto == newItem.idObjeto

        override fun areContentsTheSame(oldItem: ObjetoEquipoUI, newItem: ObjetoEquipoUI) =
            oldItem == newItem

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            EquipoItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val equipo = currentList[position]

        holder.nombreObjeto.text = context.getString(equipo.recursoNombre)
        holder.nombreObjeto.setOnClickListener {
            playSonidoBoton()
            callback.mostrarDetalleObjeto(equipo.idObjeto)
        }
        holder.btnBorrar.setOnClickListener {
            playSonidoBoton()
            callback.borrarObjeto(equipo.idPersonaje, equipo.idObjeto)
        }
    }
}

